"use strict"

/*
Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

AJAX (Asynchronous JavaScript and XML) - це технологія, що дозволяє веб-сторінці взаємодіяти з сервером без необхідності перезавантажувати її повністю. Замість цього, вона може взяти або надіслати дані на сервер асинхронно, тобто без затримок, під час яких користувач може продовжити використовувати сторінку.
Основна користь AJAX полягає в тому, що вона забезпечує більш динамічний та реактивний інтерфейс веб-додатків. Наприклад, коли користувач заповнює форму на веб-сторінці, вміст цієї форми може бути відправлений на сервер для обробки без перезавантаження сторінки. Також AJAX використовується для отримання 
та відображення динамічного контенту, такого як новини, оновлення статусів або відповідей в реальному часі.
У розробці JavaScript AJAX дуже корисний, оскільки він дозволяє виконувати запити до сервера без перезавантаження сторінки, що спрощує створення більш інтерактивних та ефективних веб-додатків. За допомогою AJAX, розробники можуть створювати веб-додатки з більшою швидкістю відгуку та зручнішим інтерфейсом для користувачів.
*/

/*
Завдання:
Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

Технічні вимоги:
Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни

Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. 
Список персонажів можна отримати з властивості characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. 
Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

Необов'язкове завдання підвищеної складності
Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
*/

const url = ' https://ajax.test-danit.com/api/swapi/films';
const filmsList = document.querySelector('#films-list');

fetch(url)
      .then(response => response.json())
      .then(data => {
        console.log(data)
        data.forEach(({ episodeId, name, openingCrawl, characters }) => {
          const filmElement = document.createElement('div');
          filmElement.innerHTML = `
            <h3>Episode ${episodeId}: ${name}</h3>
            <p>${openingCrawl}</p>
            <div id="characters-${episodeId}" class="loading-spinner"></div>
          `;

          filmsList.appendChild(filmElement);
          fetchCharacters(characters, episodeId);
        });
      })
      .catch(error => console.error('Error fetching films:', error));

   
    function fetchCharacters(characters, episodeId) {
      Promise.all(characters.map(url => fetch(url).then(response => response.json())))
        .then(charactersData => {
          const charactersContainer = document.querySelector(`#characters-${episodeId}`);
          charactersContainer.innerHTML = '<h4>Characters:</h4>';
          charactersData.forEach(({ name }) => {
            const characterName = document.createElement('p');
            characterName.textContent = name;
            charactersContainer.appendChild(characterName);
          });
          charactersContainer.classList.remove('loading-spinner');
        })
        .catch(error => console.error(`Error fetching characters for Episode ${episodeId}:`, error));
    }